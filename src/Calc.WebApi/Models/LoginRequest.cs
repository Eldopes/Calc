using System.ComponentModel.DataAnnotations;

namespace Calc.WebApi.Models
{
    /// <summary>
    /// Submit this to login existing user
    /// </summary>
    public class LoginRequest
    {
        [Required]
        public string Login { get; set; }
     
        [Required]
        public string Password { get; set; }
    }
}