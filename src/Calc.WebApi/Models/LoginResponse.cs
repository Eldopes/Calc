using System;

namespace Calc.WebApi.Models
{
    /// <summary>
    /// Response produced upon successful login
    /// </summary>
    public class LoginResponse
    { 
        /// <summary>
        /// JWT token, handled to user upon successful registration or authentication.
        /// Lifetime can be adjusted in JwtAuthorizationHandler.cs
        /// </summary>
        public string AccessToken { get; set; }
        
        public string ResultMessage { get; set; }
    }
}