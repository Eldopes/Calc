﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Calc.DataAccess;
using Calc.DataAccess.Repositories;
using Calc.WebApi.Handlers;
using Calc.WebApi.Services.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace Calc.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        { 
            // Data access
            services.AddDbContext<CalcDbContext>(options =>
            {
                options.UseNpgsql(Configuration["ConnectionStrings:Postgres"]);
            });
            services.AddScoped<IUserRepository, UserRepository>();
            
            // Authorization
            services.Configure<JwtOptions>(options =>
            {
                options.TokenIssuer = Configuration["Auth:TokenIssuer"];
                options.TokenAudience = Configuration["Auth:TokenAudience"];
                options.TokenExpiresInDays = Configuration.GetValue<int>("Auth:TokenExpiresInDays");
                options.SecurityKey = Configuration["Auth:SecurityKey"];
            });
            services.AddScoped<ITokenHandler, JwtTokenHandler>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Auth:TokenIssuer"],
                        ValidAudience = Configuration["Auth:TokenAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["Auth:SecurityKey"]))
                    };
                });
            services.AddScoped<IAuthService, AuthService>();
            services.AddTransient<IAuthorizationHandler, RoleAuthorizationHandler>();
            
            // Swagger
            services.AddSwaggerGen(c =>
            {
                // Add description on main Swagger page 
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Calc.WebApi",
                    Version = "v1",
                    Description = "WebApi Calculator"
                });
                
                // Enable Auth functionality on Swagger page
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });
            });
            
           services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calc.WebApi");
            });

           // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}