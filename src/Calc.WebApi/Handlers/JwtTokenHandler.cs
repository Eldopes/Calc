using System;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Calc.DataAccess.Models;

namespace Calc.WebApi.Handlers
{
    /// <summary>
    /// Handles JWT tokens for newly registered or logging in users
    /// </summary>
    public class JwtTokenHandler : ITokenHandler
    {
        readonly IOptions<JwtOptions> _jwtOptions;

        public JwtTokenHandler(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions;
        }
        
        /// <summary>
        /// Generates JWT token
        /// </summary>
        /// <param name="user"></param>
        /// <returns>JWT token string</returns>
        public string GenerateToken(User user)
        {
            Claim [] claims = new[]
            {
                new Claim(ClaimTypes.Name, user.Id.ToString()),
            };
            
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Value.SecurityKey));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            var token = new JwtSecurityToken(
                issuer: _jwtOptions.Value.TokenIssuer,
                audience: _jwtOptions.Value.TokenAudience,
                claims: claims,
                expires: DateTime.UtcNow.AddDays(_jwtOptions.Value.TokenExpiresInDays),
                signingCredentials: creds
            );
            
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}