using Calc.DataAccess.Models;

namespace Calc.WebApi.Handlers
{
    public interface ITokenHandler
    {
        string GenerateToken(User user);
    }
}