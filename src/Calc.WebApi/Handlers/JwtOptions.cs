namespace Calc.WebApi.Handlers
{
    /// <summary>
    /// Holds JWT configuration options from appsettings.json
    /// </summary>
    public class JwtOptions
    {
        public string TokenIssuer { get; set; }
        public string TokenAudience { get; set; }
        public int TokenExpiresInDays { get; set; }
        public string SecurityKey { get; set; }
    }
}