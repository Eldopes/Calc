using System;
using System.Linq;
using System.Threading.Tasks;
using Calc.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Calc.WebApi.Handlers
{
    public class RoleAuthorizationHandler : AuthorizationHandler<RolesAuthorizationRequirement>, IAuthorizationHandler
    {
        private readonly CalcDbContext _calcDbContext;
        
        public RoleAuthorizationHandler(CalcDbContext calcDbContext)
        {
            _calcDbContext = calcDbContext;
        }
        
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, RolesAuthorizationRequirement requirement)
        {
            if (context.User == null || !context.User.Identity.IsAuthenticated)
            {
                context.Fail();
                return;
            }

            bool found = false;
            if (requirement.AllowedRoles == null || requirement.AllowedRoles.Any() == false)
            {
                // it means any logged in user is allowed to access the resource
                found = true;
            }
            else
            {
                var userId = new Guid(context.User.Identity.Name);
                var roles = requirement.AllowedRoles;
                var roleIds = await _calcDbContext.Roles
                    .Where(p => roles.Contains(p.Name))
                    .Select(p => p.Id)
                    .ToListAsync();

                found = await _calcDbContext.Users
                    .Where(u => u.Id == userId && roleIds.Contains(u.RoleId) )
                    .AnyAsync();
            }

            if (found)
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }
        }
    }
}