namespace Calc.WebApi.Services.Auth
{
    public class AuthResult
    {
        public string Token { get; set; }
        public Status Status { get; set; }
    }

    public enum Status
    {
        Success = 0,
        UserNotFound = 1,
        WrongPassword = 2
    }
}