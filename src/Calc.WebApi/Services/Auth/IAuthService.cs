using System.Threading.Tasks;

namespace Calc.WebApi.Services.Auth
{
    public interface IAuthService
    {
         Task<AuthResult> Authenticate(string login, string password);
    }
}