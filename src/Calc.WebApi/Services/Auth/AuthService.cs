using System.Threading.Tasks;
using Calc.DataAccess.Repositories;
using Calc.WebApi.Handlers;

namespace Calc.WebApi.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly ITokenHandler _jwtTokenHandler;
        private readonly IUserRepository _userRepository;
        
        public AuthService( ITokenHandler jwtTokenHandler, IUserRepository userRepository)
        {
            _jwtTokenHandler = jwtTokenHandler;
            _userRepository = userRepository;
        }
        
        public async Task<AuthResult> Authenticate(string login, string password)
        {
            var user = await _userRepository.GetUser(login);

            if (user == null)
                return new AuthResult() {Status = Status.UserNotFound};

            if (user.Password != password)
                return new AuthResult() {Status = Status.WrongPassword};

            return new AuthResult()
            {
                Status = Status.Success,
                Token = _jwtTokenHandler.GenerateToken(user)
            };
        }
    }
}