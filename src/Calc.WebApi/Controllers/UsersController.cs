using System.Threading.Tasks;
using Calc.WebApi.Models;
using Calc.WebApi.Services.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;

namespace Calc.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class UsersController : ControllerBase
    {
        private readonly IAuthService _authService;
        
        public UsersController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Login user
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="request"></param>
        /// <response code="200">Login successful</response>
        /// <response code="404">Error: Wrong password or user not found</response>
        [HttpPost("login")]
        [SwaggerResponse(200, type: typeof(LoginResponse))] 
        public async Task <IActionResult> Login([FromBody] LoginRequest request)
        {
            var result = await _authService.Authenticate(request.Login, request.Password);
            
            return result.Status != Status.Success ? 
                StatusCode(404, new LoginResponse() { ResultMessage = result.Status.ToString()}) : 
                StatusCode(200, new LoginResponse() { AccessToken = result.Token, ResultMessage = result.Status.ToString() });
        }
    }
}