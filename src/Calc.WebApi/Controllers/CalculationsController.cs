﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Calc.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CalculationsController : ControllerBase
    {
        public CalculationsController()
        {
        }

        /// <summary>
        /// Add numbers.
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <response code="200"></response>
        [HttpGet("add")]
        [Authorize(Roles = "Pro")]
        public IActionResult Add([FromQuery] double a, double b)
        {
            var result = a + b;
            return StatusCode(200, result);
        }

        /// <summary>
        /// Subtract numbers.
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <response code="200"></response>
        [HttpGet("subtract")]
        [Authorize(Roles = "Noob,Casual,Pro")]
        public IActionResult Subtract([FromQuery] double a, double b)
        {
            var result = a - b;
            return StatusCode(200, result);
        }

        /// <summary>
        /// Multiply numbers.
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <response code="200"></response>
        [HttpGet("multiply")]
        [Authorize(Roles = "Noob,Casual,Pro")]
        public IActionResult Multiply([FromQuery] double a, double b)
        {
            var result = a * b;
            return StatusCode(200, result);
        }

        /// <summary>
        /// Divide numbers.
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <response code="200"></response>
        [HttpGet("divide")] 
        [Authorize(Roles = "Noob,Casual,Pro")]
        public IActionResult Divide([FromQuery] double a, double b)
        {
            var result = a / b;
            return StatusCode(200, result);
        }
    }
}