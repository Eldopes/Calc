using Calc.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Calc.DataAccess
{
    public class CalcDbContext : DbContext
    { 
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        
        public CalcDbContext(DbContextOptions<CalcDbContext> options)
            : base(options)
        {
        }
      
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var user = builder.Entity<User>();
            user.HasKey(e => e.Id);
            user.Property(e => e.Id).HasColumnName("Id").IsRequired();
            user.Property(e => e.Login).HasColumnName("Login").IsRequired();
            user.Property(e => e.Password).HasColumnName("Password").IsRequired();
            user
                .HasOne(u => u.Role)
                .WithMany(r => r.Users)
                .HasForeignKey(u => u.RoleId);
        }
    }
}