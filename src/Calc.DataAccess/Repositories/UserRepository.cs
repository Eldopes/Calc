﻿using System.Threading.Tasks;
using Calc.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Calc.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly CalcDbContext _context;
        public UserRepository(CalcDbContext context)
        {
            _context = context;
        }

        public async Task <User> GetUser(string login)
        {
            var user = await _context.Users
                .Include(u => u.Role)
                .FirstOrDefaultAsync(u => u.Login == login);

            return user;
        }
    }
}