using System.Threading.Tasks;
using Calc.DataAccess.Models;

namespace Calc.DataAccess.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUser(string login);
    }
}