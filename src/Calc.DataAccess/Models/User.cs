using System;

namespace Calc.DataAccess.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; } // Should be hashed in production 
        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }
}