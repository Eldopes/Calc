using System;
using FluentMigrator;

namespace Calc.Migrations.Migrations
{
    [Migration(20190822000001)]
    public class AddUsersTable : Migration
    {
        public override void Up()
        {
            Create.Table("Users")
                .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("Login").AsString()
                .WithColumn("Password").AsString()
                .WithColumn("RoleId").AsGuid();

            Insert.IntoTable("Users")
                .Row(new
                {
                    Id = Guid.NewGuid(), Login = "BigBoy", Password = "big_boy",
                    RoleId = new Guid("5ba29213-03f6-4bd1-8e87-487e3bbc9574") // Pro (can use all methods)
                }) 
                .Row(new
                {
                    Id = Guid.NewGuid(), Login = "SmartAss", Password = "smart_ass",
                    RoleId = new Guid("ddd25304-79b4-4213-a2b6-6f22bcf54fb3") // Casual (cannot use Add)
                }) 
                .Row(new
                {
                    Id = Guid.NewGuid(), Login = "RetardedKid", Password = "retarded_kid",
                    RoleId = new Guid("0ce33a26-4883-4730-9b11-f682d5b00c55") // Noob (cannot use Add)
                }) 
                .Row(new
                {
                    Id = Guid.NewGuid(), Login = "StupidMonkey", Password = "stupid_monkey",
                    RoleId = new Guid("0ce33a26-4883-4730-9b11-f682d5b00c55") // Noob (cannot use Add)
                }); 
        }

        public override void Down()
        {
            Delete.Table("Users");
        }
    }
}