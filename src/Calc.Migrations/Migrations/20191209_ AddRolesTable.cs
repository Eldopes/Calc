using System;
using FluentMigrator;

namespace Calc.Migrations.Migrations
{
    [Migration(20190822000002)]
    public class AddRolesTable : Migration
    {
        public override void Up()
        {
            Create.Table("Roles")
                .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("Name").AsString();

            Insert.IntoTable("Roles")
                .Row(new {Id = new Guid("5ba29213-03f6-4bd1-8e87-487e3bbc9574"), Name = "Pro"})
                .Row(new {Id = new Guid("ddd25304-79b4-4213-a2b6-6f22bcf54fb3"), Name = "Casual"})
                .Row(new {Id = new Guid("0ce33a26-4883-4730-9b11-f682d5b00c55"), Name = "Noob"});
        }

        public override void Down()
        {
            Delete.Table("Roles");
        }
    }
}