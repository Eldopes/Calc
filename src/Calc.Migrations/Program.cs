﻿using System;
using System.IO;
using System.Threading;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Calc.Migrations.Migrations;
using Npgsql;

namespace Calc.Migrations
{
    class Program
    {
        private static IConfiguration _config;
        
        static void Main(string[] args)
        {
            _config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development"}.json", optional: true)
                .Build();
            
            var serviceProvider = CreateServices();

            Console.WriteLine("Waiting 10 seconds for DB startup");
            Thread.Sleep(10000);
                
            Policy.Handle<NpgsqlException>().WaitAndRetry(
                    15,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt * 0.5)),
                    (exception, timeSpan, retryCount, context) =>
                    {
                        Console.WriteLine($"Attempt to connect number {retryCount} after the error {exception}, the next attempt in {timeSpan}");
                    })
                .Execute(() =>
                {
                    using (var scope = serviceProvider.CreateScope())
                    {
                        UpdateDatabase(scope.ServiceProvider);
                    }
                    
                });
        }
       
        private static IServiceProvider CreateServices()
        {
            return new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddPostgres()
                    .WithGlobalConnectionString(_config["ConnectionStrings:Postgres"])
                    .ScanIn(typeof(AddUsersTable).Assembly).For.Migrations())
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);
        }

        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();
            
            runner.MigrateUp();
        }
    }
}