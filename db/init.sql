CREATE SCHEMA IF NOT EXISTS public;

CREATE EXTENSION IF NOT EXISTS dblink;
DO
$do$
    BEGIN
        IF EXISTS (SELECT 1 FROM pg_database WHERE datname = 'Calc') THEN
            RAISE NOTICE 'Database already exists';
        ELSE
            PERFORM dblink_exec('dbname=' || current_database()
                        , 'CREATE DATABASE "Calc"');
        END IF;
    END
$do$;